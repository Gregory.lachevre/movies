<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Commandes utiles

php artisan key:generate

php artisan migrate

php artisan create:movie pour insérer des données de l'api dans la base de données

Ajouter une variable dans le .env API_KEY


## Fonctionnalités du projet

- Une page qui liste les films tendance du mois ou de la journée + gestion erreur

- Une page de visualisation des informations du film grâce à l'api + gestion erreur

- Une page qui liste l'ensemble des films présents en base de données avec la possibilité de les supprimer

- Installation de laravel sail (non fonctionnel)

## Mon environnement de travail

- Laragon


