<?php

namespace App\Console\Commands;

use App\Models\Movie;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;


class CreateMovieCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'movie:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create movie';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $i = 1;
        $apiKey = env('API_KEY');

        for ($i; $i <= 40; $i++) {


            $api_url = "https://api.themoviedb.org/3/movie/$i?api_key=$apiKey&language=en-US";

            $response = Http::get($api_url);

            if ($response->status() == 404) {
                $i++;
            } else {
                $data = json_decode($response->body());

                Movie::create([
                    'id' => $data->id,
                    'title' => $data->title,
                    'overview' => $data->overview,
                    'poster_path' => $data->poster_path ?: 1,
                    'release_date' => $data->release_date,

                ]);
            }

        }
    }
}
