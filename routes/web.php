<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');

    Route::get('movie', [\App\Http\Controllers\MovieController::class, 'index'])->name('movie.index');
    Route::get('movie/{movie:id}', [\App\Http\Controllers\MovieController::class, 'show'])->name('movie.show');
    Route::get('api/movie', [\App\Http\Controllers\MovieController::class, 'trendMoviesApi'])->name('movie.list');
    Route::get('api/show', [\App\Http\Controllers\MovieController::class, 'showApi'])->name('movie.showApi');
    Route::delete('movie/delete/{movie:id}', [\App\Http\Controllers\MovieController::class, 'destroy'])->name('movie.destroy');
});


